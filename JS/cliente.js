let cliente = JSON.parse(localStorage.getItem('cliente'));

const templateCliente = document.getElementById('template-cliente');

const fragment = document.createDocumentFragment();

document.addEventListener('DOMContentLoaded', () => {
    pintarCliente()
})

const pintarCliente = () => {
    let clienteHtml = `
    <div class="container mt-5">
        <div class="outer-rectangle">
          <!-- Círculo con imagen -->
          <div class="circle">
            <img src="" alt="">
          </div>
      
          
          <h4 class="mt-3">${cliente.Nombre} ${cliente.Apellidos}</h4>
      
          <div class="inner-rectangle mt-3">
            <p><strong>Email:  </strong>${cliente.Email}</p>
            <p><strong>Fecha de Nacimiento:  </strong>${cliente["Fecha Nacimiento"]}</p>
            <p><strong>Número Celular:  </strong>${cliente.Celular}</p>
          </div>
        </div>
      </div>
    `
    templateCliente.innerHTML = clienteHtml;
}
